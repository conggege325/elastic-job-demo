package com.mysite.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.mysite.task.job.SchedulerSimpleJob;
import com.mysite.task.service.ElasticJobService;

@Component
public class ApplicationStarted implements CommandLineRunner {

    protected static final Logger logger = LoggerFactory.getLogger(ApplicationStarted.class);

    @Autowired
    private ElasticJobService elasticJobService;

    @Override
    public void run(String... args) throws Exception {
        elasticJobService.addSimpleJobScheduler(SchedulerSimpleJob.class, "0/10 * * * * ?", 2, "0=A,1=B");
        logger.info("Application is started Successfully !");
    }
}
