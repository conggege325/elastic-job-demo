package com.mysite.task.job;

import javax.jms.Queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;

@Component
public class SchedulerSimpleJob implements SimpleJob {

    protected static final Logger logger = LoggerFactory.getLogger(SchedulerSimpleJob.class);

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    public Queue queue = new ActiveMQQueue("task-queue");

    @Override
    public void execute(ShardingContext shardingContext) {

        Long num = redisTemplate.opsForValue().increment("counter", 1);
        jmsMessagingTemplate.convertAndSend(queue, "taskid: " + num);
        logger.info("taskid: " + num);
        /*
         * logger.debug(String.format("------Thread ID: %s, 任务总片数: %s, " +
         * "当前分片项: %s.当前参数: %s," + "当前任务名称: %s.当前任务参数: %s",
         * Thread.currentThread().getId(), shardingContext.getShardingTotalCount(),
         * shardingContext.getShardingItem(), shardingContext.getShardingParameter(),
         * shardingContext.getJobName(), shardingContext.getJobParameter()));
         */
    }
}
